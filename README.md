# Vanillia Minecraft
[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-908a85?logo=gitpod)](https://gitpod.io/from-referrer/)
This modpack does not add any actual content to the game and is compliant with most server's mod policies.

### Download
[![](https://cf.way2muchnoise.eu/title/583252.svg)](https://www.curseforge.com/minecraft/modpacks/vanillia), [Technic](https://www.technicpack.net/modpack/vanillia-118.1904742),
[MultiMC](https://gitlab.com/Merith-TK/vanillia-modpack/-/archive/main/vanillia-modpack-main.zip)

## Disclaimer
I am not responsible for server bans, please check with server admins before using this modpack, especially on hypixel

## Mods Included
* [AntiGhost](https://www.modrinth.com/mod/antighost)
	* Adds a keybind to re-request block information around you. helpful when there is a "Ghost Block" in your way,
* [Architechury](https://www.curseforge.com/minecraft/mc-mods/architectury-fabric)
	* API used for mods to make building for forge/fabric easier
* [BadSTDOut](https://modrinth.com/mod/badstdout)
	* Makes the logs state which mod is using a `system.out` call,
* [BetterF3](https://modrinth.com/mod/betterf3)
	* Just a better F3,
* [Better Loading Screen](https://www.curseforge.com/minecraft/mc-mods/betterloadingscreen)
	* Brings back the old forge 1.12.2 loading screen
* [BlockMeter](https://www.curseforge.com/minecraft/mc-mods/blockmeterfabric)
	* Client Side measuring tape for building
* [BlockMixer](https://www.curseforge.com/minecraft/mc-mods/blockmixer-fabric)
	* Shuffles blocks in your hand when building to give randomness
* [Blur](https://modrinth.com/mod/blur-fabric)
	* Blurs the background of menus
* [Bobby](https://modrinth.com/mod/bobby)
	* Cache's chunks locally so that you can have a Render Distance further than what a server would allow
* [Chunky](https://modrinth.com/mod/chunky)
	* Chunk Pregenerator for Single Player
* [CIT Resewn](https://modrinth.com/mod/cit-resewn)
	* Optifine's Custom Item Textures
* [Charmonium](https://modrinth.com/mod/charmonium)
	* Ambient Sound Overhaul,
* [Clear Despawn](https://modrinth.com/mod/cleardespawn)
	* Makes Items that are about to despawn more obvious
* [Cloth Config](https://www.curseforge.com/minecraft/mc-mods/cloth-config)
	* Config API used by many mods
* [Clumps](https://www.curseforge.com/minecraft/mc-mods/clumps)
	* Clumps xp orbs together
* [Continuity](https://modrinth.com/mod/continuity)
	* Connected Textures
* [Dark Loading Screen](https://www.curseforge.com/minecraft/mc-mods/dark-loading-screen)
	* Makes minecraft loading screen bareable
* [Entity Culling](https://www.curseforge.com/minecraft/mc-mods/entityculling)
	* Makes Unseen entities not render anyways
* [Essentials](https://essential.gg)
	* The essential multiplayer mod for Minecraft Java.
	* Invite friends to your singleplayer worlds.
	* Customize your Character with cosmetics
	* Connect with friends 
		* Friends system 
		* Private Messaging system
	* Featured Server Browser
* [Fabric API](https://modrinth.com/mod/fabric-api)
	* Needed Library for most of the mods here
* [Fabrishot](https://modrinth.com/mod/fabrishot)
	* Obsurdly Large Screenshots, Because why the hell not
* [Ferrite Core](https://modrinth.com/mod/ferrite-core)
	* RAM Usage Reducer
* [Figura](https://modrinth.com/mod/figura)
	* custom 3D player models with potential for HD Skins.
* [First Person Model](https://www.curseforge.com/minecraft/mc-mods/first-person-model)
	* First Person persepctive mod
* [Grid](https://modrinth.com/mod/grid)
	* Displays a grid on the ground so you can place blocks in a pattern
* [HudTweaks](https://modrinth.com/mod/hudtweaks)
	* Grants full control over your HUD
* [Indium](https://modrinth.com/mod/indium)
	* Fixes some issues with sodium+iris
* [Inspecio](https://modrinth.com/mod/inspecio)
	* Usefull tooltips
* [Inventory HUD](https://www.curseforge.com/minecraft/mc-mods/inventory-hud-forge)
	* Display your Inventory on your HUD
* [Iris Shaders](https://modrinth.com/mod/iris)
	* Better Shaders mod for Fabric
	* Performs better than optifine
	* Doesnt crash
	* Compatible with most popular optifine shaders
* [Lambda Better Grass](https://modrinth.com/mod/lambdabettergrass)
	* makes grass and snow look nicer
* [Lamb Dynamic Lights](https://www.modrinth.com/mod/lambdynamiclights)
	* Dynamic Lighting, when holding a torch, the area around you lights up
* [Lithium](https://modrinth.com/mod/lithium)
	* Basic Performance booster, 
	* Useless for multiplayer, but helpful for singleplayer
* [MinecraftCapes](https://minecraftcapes.net/)
	* Custom (***free***) Capes for Minecraft
* [ModMenu](https://modrinth.com/mod/modmenu)
	* Its the Mod Menu, kinda needed
* [Mouse Wheelie](https://modrinth.com/mod/mouse-wheelie)
	* A small clientside mod to enable various mouse wheel related actions. Features item scrolling, inventory sorting, item refilling and much more!
* [Nimble](https://www.curseforge.com/minecraft/mc-mods/nimble-fabric)
	* Smooth third person animations
* [Not Enough Animations](https://www.curseforge.com/minecraft/mc-mods/not-enough-animations)
	* various animation tweaks to make things just look right
* [OK Zoomer](https://modrinth.com/mod/ok-zoomer)
	* Zoom Mod
* [OpenAuth](https://github.com/RaphiMC/OpenAuthMod)
	* Allows using proxies to connect to servers,
* [Perspective Mod Redux](https://www.curseforge.com/minecraft/mc-mods/perspective-mod-redux)
	* 360 degree pivot third person camera
* [Plasmo Voice](https://modrinth.com/mod/plasmo-voice)
	* Proximity voicechat for servers that support it
	* ***SERVER ADMINS: THERE IS A PLUGIN FOR SERVER SUPPORT!***
* [RoughlyEnoughItems](https://www.curseforge.com/minecraft/mc-mods/roughly-enough-items)
	* A general Purpose Inventory mod needed for many modpacks, also helpful for just plain vanillia minecraft as you can search up recipies on the fly
* [Sodium](https://modrinth.com/mod/sodium)
	* Where Iris gets its performance boosts for base game
* [Sodium Extra](https://modrinth.com/mod/sodium-extra)
	* Extra Options for sodium
* [Starlight](https://modrinth.com/mod/starlight)
	* Lighting Engine OVERHAUL
* [Stendhal](https://modrinth.com/mod/stendhal)
	* Better Book and Sign writing
* [ViaFabric](https://www.curseforge.com/minecraft/mc-mods/viafabric)
	* Allows joining older minecraft servers
* [Wildfires Gender Mod](https://modrinth.com/mod/female-gender)
	* Adds configurable breasts for players... thats it
* [Xaeros Minimap (Fair Play)](https://www.curseforge.com/minecraft/mc-mods/xaeros-minimap-fair-play-edition)
	* [Xaeros Minimap](https://www.curseforge.com/minecraft/mc-mods/xaeros-minimap), but fair for PVP and Survival Servers
* [Xaeros World Map](https://www.curseforge.com/minecraft/mc-mods/xaeros-world-map)
	* A good world map that integrates with Xaeros Minimap
* [Your Options Shall Be Respected](https://www.curseforge.com/minecraft/mc-mods/yosbr)
	* Lets me push custom premade configs to the modpack without overriding your own

## Missing from 1.18.2
* [Armor Visibility](https://modrinth.com/mod/armor-visibility)
	* Toggle Hiding armor clientside
* [C2ME](https://www.curseforge.com/minecraft/mc-mods/c2me-fabric)
	* Chunk Performance Enhancer
* [Illuminations](https://www.curseforge.com/minecraft/mc-mods/illuminations)
	* Adds fancy ambient particles!
* [Paxi](https://www.curseforge.com/minecraft/mc-mods/paxi-fabric)
	* Automatic Resource and Datapacks
